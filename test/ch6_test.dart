import 'dart:typed_data';

import 'package:dart_bitcoin/helpers/hex.dart';
import 'package:dart_bitcoin/script.dart';
import 'package:dart_bitcoin/script/num.dart';
import 'package:dart_bitcoin/script/opcode.dart';
import 'package:dart_bitcoin/script/operations.dart';
import 'package:dart_bitcoin/script/operations/op_add.dart';
import 'package:dart_bitcoin/script/operations/op_checksig.dart';
import 'package:dart_bitcoin/script/operations/op_dup.dart';
import 'package:dart_bitcoin/script/operations/op_equal.dart';
import 'package:test/test.dart';

void main() {
  group('Ch6: Script', () {
    test('op_has256', () {
      OpCode op = OpCode.OP_HASH256;
      List<int> stack = List.from([20, 30, 40]);

      bool success = operations[op]!(stack);

      expect(success, isTrue);
      expect(stack.length, equals(3));
      // expect(stack, equals(List.from([20, 30, ])));
    });

    test('script parse', () {
      // 19 76 a9 14 ab0c0b2e98b1ab6dbf67d4750b0a56244948a879 88 ac
      String scriptString = '1976a914ab0c0b2e98b1ab6dbf67d4750b0a56244948a87988ac';
      Uint8List scriptBytes = hexStringToBytes(scriptString);
      int baseIndex = 0;
      Script script;
      (script, baseIndex) = Script.parseFromBytes(scriptBytes, baseIndex);
      print(script);
      expect(script.toString(),
          equals('OP_DUP OP_HASH160 ab0c0b2e98b1ab6dbf67d4750b0a56244948a879 OP_EQUALVERIFY OP_CHECKSIG '));
      expect(script.serialize(), scriptString);
    });

    test('Script execution: Arithmetic', () {
      // 4 20
      Script script = Script([
        OperationCmd(OpCode.OP_EQUAL),
        ElementCmd(encodeNum(BigInt.from(9))),
        OperationCmd(OpCode.OP_ADD),
        ElementCmd(encodeNum(BigInt.from(5))),
        ElementCmd(encodeNum(BigInt.from(4))),
      ]);

      script.evaluate(Uint8List.fromList([]));
    });

    test('opDup', () {
      List<ScriptCmd> stack = [
        ElementCmd(Uint8List.fromList([0x00])),
        ElementCmd(Uint8List.fromList([0x01])),
        ElementCmd(Uint8List.fromList([0x02])),
      ];
      opDup(stack);
      expect(stack.length, equals(4));
      expect((stack[3] as ElementCmd).element, equals(Uint8List.fromList([0x02])));
    });

    test('opAdd', () {
      List<ScriptCmd> stack = [
        ElementCmd(encodeNum(BigInt.from(999))),
        ElementCmd(encodeNum(BigInt.from(999))),
      ];
      opAdd(stack);
      expect(stack.length, equals(1));
      expect((stack[0] as ElementCmd).element, equals(encodeNum(BigInt.from(1998))));
    });

    test('opEqual', () {
      List<ScriptCmd> stack = [
        ElementCmd(encodeNum(BigInt.from(999))),
        ElementCmd(encodeNum(BigInt.from(999))),
      ];
      opEqual(stack);
      expect(stack.length, equals(1));
      expect((stack[0] as ElementCmd).element, equals(encodeNum(BigInt.one)));
    });

    test('opCheckSig', () {
      List<ScriptCmd> stack = [
        ElementCmd(Uint8List.fromList([0x00])),
        ElementCmd(Uint8List.fromList([0x01])),
      ];
      Uint8List z = Uint8List.fromList([0x00]);
      opCheckSig(stack, z);
      print('opCheckSig');
    });
  });
}
