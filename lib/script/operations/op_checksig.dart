import 'dart:typed_data';

import 'package:dart_bitcoin/helpers/uint8list.dart';
import 'package:dart_bitcoin/script.dart';

bool opCheckSig(List<ScriptCmd> stack, Uint8List z) {
  if (stack.isEmpty) {
    return false;
  }
  dynamic pubKey = stack.removeLast();
  dynamic sig = stack.removeLast();

  return true;
}
