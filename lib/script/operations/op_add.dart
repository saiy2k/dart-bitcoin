import 'dart:typed_data';

import 'package:dart_bitcoin/helpers/bigint_util.dart';
import 'package:dart_bitcoin/script.dart';

bool opAdd(List<ScriptCmd> stack) {
  if (stack.isEmpty) {
    return false;
  }
  ScriptCmd el1 = stack.removeLast();
  ScriptCmd el2 = stack.removeLast();
  if (el1 is! ElementCmd || el2 is! ElementCmd) {
    return false;
  }
  BigInt n1 = readBytes(Uint8List.fromList(el1.element.reversed.toList()));
  BigInt n2 = readBytes(Uint8List.fromList(el2.element.reversed.toList()));
  stack.add(ElementCmd(Uint8List.fromList(writeBigInt(n1 + n2).reversed.toList())));
  return true;
}
