import 'package:dart_bitcoin/script.dart';

bool opDup(List<ScriptCmd> stack) {
  if (stack.isEmpty) {
    return false;
  }
  stack.add(stack.last);
  return true;
}
