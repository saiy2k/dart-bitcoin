import 'dart:typed_data';

Uint8List encodeNum(BigInt num) {
  if (num == BigInt.zero) return Uint8List.fromList([]);

  BigInt absNum = num.abs();
  bool negative = num < BigInt.zero;

  BytesBuilder buffer = BytesBuilder();
  Uint8List result = Uint8List.fromList([]);

  while (absNum > BigInt.zero) {
    buffer.addByte(absNum.toUnsigned(8).toInt());
    absNum = absNum >> 8;
  }

  if (buffer.toBytes().last & 0x80 != 0) {
    if (negative) {
      buffer.addByte(0x80);
    } else {
      buffer.addByte(0x00);
    }
    result = buffer.toBytes();
  } else if (negative) {
    result = buffer.toBytes();
    result.last = result.last | 0x80;
  } else {
    result = buffer.toBytes();
  }

  return result;
}

BigInt decodeNum(Uint8List bytes) {
  return BigInt.from(0);
}
